{{/*
Expand the name of the chart.
*/}}
{{- define "effigeo-chart.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "effigeo-chart.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "effigeo-chart.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "effigeo-chart.labels" -}}
helm.sh/chart: {{ include "effigeo-chart.chart" . }}
{{ include "effigeo-chart.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "effigeo-chart.selectorLabels" -}}
app.kubernetes.io/name: {{ include "effigeo-chart.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "effigeo-chart.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "effigeo-chart.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{- define "helpers.list-env-variables"}}
 {{- if  .Values.app.env.secret}}
     {{- end }}
 {{- if  .Values.app.env.configmap}}
     {{- end }}


{{- range $key, $val, $source := .Values.app.env }}
- name: {{ $key }}
  valueFrom:
    {{- if contains $source "configmap"}}
    configMapKeyRef:
      name: {{  include "effigeo-chart.fullname" $ }}
      key: {{ $key }}
    {{- end }}
    {{- if contains $source "secret"}}
    secretRef:
      name: {{  include "effigeo-chart.fullname" $ }}
      # key: {{ $key }}
    {{- end }}
{{- end}}
{{- end }}


