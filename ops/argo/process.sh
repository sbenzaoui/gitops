# cd dev
# mkdir dev/${CI_COMMIT_BRANCH}

# Color Constants
COL_PRIMARY="\e[1m\e[92m"
COL_SECONDARY="\e[33m"
COL_DANGER="\e[31m"
COL_SUCCESS="\e[32m"
COL_RESET="\e[0m"

# Prints stylized primary text
echo_primary() {
    printf "${COL_PRIMARY}=> %b${COL_RESET}\n" "$*"
}


# CHANGES=$(git status --porcelain | wc -l)
# echo $CHANGES

# if [ "$CHANGES" -gt "0" ]; then
DIR=${CI_COMMIT_BRANCH}

# create the dir, then the file
mkdir -p "dev/${DIR}" && touch "dev/${DIR}/apps.yaml"

# show result
# ls -l "$FILEPATH"
rm -f dev/${CI_COMMIT_BRANCH}/final.yaml temp.yaml
( echo "cat <<EOF >dev/${CI_COMMIT_BRANCH}/apps.yaml";
  cat template.yaml;
  echo "EOF";
) >temp.yaml
. temp.yaml
cat  dev/${CI_COMMIT_BRANCH}/apps.yaml
git add -A && git commit -m "deploy dev branch ${CI_COMMIT_BRANCH}" \
    && git push -u origin dev -o ci.skip
# else
# echo "no changes in branch"
# fi