#!/bin/bash
# Color Constants
COL_PRIMARY="\e[1m\e[92m"
COL_SECONDARY="\e[33m"
COL_DANGER="\e[31m"
COL_SUCCESS="\e[32m"
COL_RESET="\e[0m"

# Prints stylized primary text
echo_primary() {
    printf "${COL_PRIMARY}=> %b${COL_RESET}\n" "$*"
}

# Entrypoint
build_image() {
    echo_primary "Login to the gitlab registry"
    login_to_registry

    echo_primary "Build docker image"
    build_docker_image

    echo_primary "Tag and push"
    if [[ "$CI_COMMIT_TAG" ]]; then
        # If a git tag is available, tag the docker image with both `:<git_tag>` and `:latest`
        tag_and_push "$CI_COMMIT_TAG"
        tag_and_push "latest"
    else
        # Otherwise, tag the docker image with `:<git_commit_sha>`
        tag_and_push "$CI_COMMIT_SHORT_SHA"
    fi
}

# Logs into the gitlab registry
login_to_registry() {
    echo "$CI_JOB_TOKEN" | docker login -u gitlab-ci-token --password-stdin "$CI_REGISTRY"
}

# Build the docker image, tagged "<project>:latest"
build_docker_image() {
    # shellcheck disable=2086
    docker build --pull . \
        --tag "$CI_PROJECT_NAME:latest" \
        --build-arg "REGISTRY=$CI_REGISTRY" \
        --build-arg "RELEASE=${CI_COMMIT_TAG:-$CI_COMMIT_SHORT_SHA}" \
        $DOCKER_BUILD_ARGS
}

# Tags the <project>:latest image with the given tag and pushes it
tag_and_push() {
    local docker_tag="$1"
    local docker_image="$CI_REGISTRY_IMAGE:$docker_tag"

    echo_secondary "$docker_image"
    docker tag "$CI_PROJECT_NAME:latest" "$docker_image"
    docker push "$docker_image"
}

build_image
